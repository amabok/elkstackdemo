# Elk stack demo

# Pre-requisites
* Recent version of Java installed
* Recent Docker version with docker-compose installed
* Filebeat installed on your DEV machine: https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html

# Compiling the source
- from the command line: gradlew clean build

# Running
* cd to the project root directory
* make sure you have:
	* docker up and running
	* ELK stack up and running by running: docker-compose up elk  
	* edited the application.properties and set the logging.path property to a suitable location that you control
	* edited the filebeat.yml so that the path of the logs to be processed match the path provided in the application.properties
	* edited the filebeat.yml so that the path of the certificate_authorities is created
	* copied the logstash-beats.crt file in the root of the project to the path edited before
	* copied the filebeat.yml file previously edited to the proper system configuration folder (e.g. for Linux: /etc/filebeat )
	* started the filebeat service (e.g. for Linux: sudo /etc/init.d/filebeat start)
 	* run the app from the command line: gradlew bootRun

# Stopping
* sudo /etc/init.d/filebeat stop
* docker-compose down

# Log dashboard
* After the app is running open your browser and point it to: http://localhost:5601/app/kibana#/discover