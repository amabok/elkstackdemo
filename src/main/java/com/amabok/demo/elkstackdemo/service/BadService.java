package com.amabok.demo.elkstackdemo.service;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service

public class BadService {

	private Logger log = LoggerFactory.getLogger(BadService.class);
	
	@Scheduled(fixedDelay = 3000)
	public void doSomethingBad() {
			
		Random rand = new Random();

		int  randomNumber = rand.nextInt();
		
		log.error("Oops!!! Something bad happened:  {}", randomNumber);
	}
}
