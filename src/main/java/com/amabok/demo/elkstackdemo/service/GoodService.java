package com.amabok.demo.elkstackdemo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class GoodService {

	private Logger log = LoggerFactory.getLogger(GoodService.class);
	
	
	@Scheduled(fixedDelay = 2000)
	public void doSomething() {
		log.info("Just doing my unsuspecting normal stuff, move along... nothing to see here...");
	}
}
