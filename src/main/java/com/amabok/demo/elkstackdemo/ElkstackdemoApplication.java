package com.amabok.demo.elkstackdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElkstackdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElkstackdemoApplication.class, args);
	}
}